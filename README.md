# Simple NodeJS Webapp

Requirements: node:12-alpine

Execute Simple Node Webapp:
```
npm ci --only=production
node webapp.js
```

Webapp will be accessible on port 8080
